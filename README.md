###################
CLONAGE - cloner entièrement un serveur avec rsync :
1. Sur la machine cible : Booter sur un live cd linux, relever l'ip donné par le dhcp et configurer un mot de passe root.

2. Connexion ssh  depuis la source vers la cible.

3. Partitionnement du disque sur lequel on veut installer notre clône.

4. Formatage des partitions.

5. Création d'un répertoire qui accueillera la racine de notre futur clône.

6. Montage de la partition racine sur ce répertoire.

7. Création de l'arborescence du système : un sous répertoire par partition.

8. Montage de chacune des  partitions dans leurs répertoires respectifs.

9. Copie de la racine source vers la racine cible en excluant /proc/* /sys/* /dev/* /run/* car ces répertoires sont chargés au démarrage et leur présence en doublon provoquerait un kernel panic.

10. Montage du système live chargé en mémoire vers la racine de notre futur clône : mount -o bind /proc cloneRepRacine/proc ; mount -o bind /sys cloneRepRacine/sys ; mount -o bind /dev cloneRepRacine/dev ; mount -o bind /run cloneRepRacine/run.

11. Se chrooter dans /clone. Le chroot permet de s'emprisonner dans un répertoire, le système croyant ainsi que nous nous trouvons à la racine du système de fichier. À partir de cette étape c'est comme si nous étions dans un deuxième système d'exploitation se trouvant à l'intérieur du premier.

12. Mise à jour du fichier /etc/fstab pour le montage des partitions au démarrage : Il est conseillé de renseigner les UUID au lieu des  noms de partitions tels que /dev/sda. Ceci pour qu'il n'y ait aucune confusion entre les périphériques. En effet l'UUID est un numéro hexadécimal inscrit sur le superblock de la partition lors du formatage. La commande blkid permet de connaître l'uuid d'une partition.

13. Installation du chargeur d'amorçage « grub » sur le disque par apt, grub-install et update-grub. Attention aux différents grub : grub-pc pour versions bios, grub-efi-amd64 pour efi (d'où la réinstallation apt si nécessaire). 

14. Suppression du fichier etc/udev/rules.d/70-persistent-cd.rules : ce fichier fait l'association entre les adresses physiques des cartes réseaux et les interfaces, le fichier sera recréé au redémarrage avec les bonnes adresses physiques.

15. Changement du hostname dans /etc/hostname.

16. Mise en place d'une ip fixe dans /etc/network/interfaces

17. Sortir du chroot, retirer le live cd, redémarrer la machine, celle-ci boote maintenant sur le disque dur, notre clône est opérationnel !
#####################
